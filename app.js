var express     = require("express"),
    app         = express(),
    bodyParser  = require("body-parser")


app.use(bodyParser.urlencoded({extended: true}));


//Set view engine
app.set("view engine", "ejs");

app.get("/", function(req, res){
    res.render("landing");
});

app.get("/managedITservices", function(req, res){
//render services page
  res.render("managedITservices");
});
app.get("/professionalITservices", function(req, res){
//render services page
res.render("professionalITservices");
});
app.get("/technicalDocumenting", function(req, res){
//render documenting page
res.render("technicalDocumenting");
});
app.get("/basic", function(req, res){
//render basic page
res.render("basic");
});
app.get("/homeServices", function(req, res){
//render home services page
res.render("homeServices");
});
app.get("/pro", function(req, res){
//render pro page
res.render("pro");
});
app.get("/premium", function(req, res){
//render premium page
res.render("premium");
});
app.get("/pricing", function(req, res){
//render pricing page
res.render("pricing");
});
//Tell Express to listen for requests (start server)

app.use(express.static('public'));

app.listen(3000, "0.0.0.0", function() {
        console.log("Server has started!!!");
        });


